# Copyright 2014-2015 Michael Jones.
# Distributed under the terms of the GNU General Public License v2

EAPI="5"

inherit git-r3 qmake-utils

DESCRIPTION="High level qt bindings to the zeromq networking library"
HOMEPAGE="https://github.com/jonesmz/qtzeroconf"
#SRC_URI="https://github.com/jonesmz/qtzeroconf/archive/version-${PV}.tar.gz"
EGIT_REPO_URI="https://github.com/jonesmz/qtzeroconf.git"

LICENSE="LGPL-2.1"
SLOT="0"
KEYWORDS="x86 amd64"
IUSE=""

REQUIRED_USE=""

COMMON_DEPEND="
	dev-qt/qtcore:5
	net-dns/avahi
"

DEPEND="
	${COMMON_DEPEND}
"

RDEPEND="
	${COMMON_DEPEND}
"
	
src_configure() {
	eqmake5 qtzeroconf.pro
}

src_install() {
        dolib bin/*
        doheader -r include/qtzeroconf/
        insinto "/usr/$(get_libdir)/pkgconfig"
        newins "pkgconfig/${PN}-service.pc" "${PN}-service.pc"
        newins "pkgconfig/${PN}-browser.pc" "${PN}-browser.pc"
        newins "pkgconfig/${PN}-common.pc" "${PN}-common.pc"                                                                                                                                                                                   
}

