# Copyright 2014-2015 Michael Jones.
# Distributed under the terms of the GNU General Public License v2

EAPI="5"

inherit qmake-utils

DESCRIPTION="High level qt bindings to the zeromq networking library"
HOMEPAGE="https://github.com/jonesmz/nzmqt/"
SRC_URI="https://github.com/jonesmz/nzmqt/archive/version-${PV}.tar.gz"

LICENSE="BSD"
SLOT="0"
KEYWORDS="x86 amd64"
IUSE=""

REQUIRED_USE=""

COMMON_DEPEND="
	dev-qt/qtcore:5
	>=net-libs/zeromq-3.0.0
"

DEPEND="
	${COMMON_DEPEND}
"

RDEPEND="
	${COMMON_DEPEND}
"
	
src_unpack() {
	unpack ${A}
	mv ${PN}-version-${PV} ${P}
}

src_configure() {
        ./setup-project.sh
	eqmake5 src/nzmqt_sharedlib.pro
}
