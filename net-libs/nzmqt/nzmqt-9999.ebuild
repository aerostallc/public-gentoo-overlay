# Copyright 2014-2015 Michael Jones.
# Distributed under the terms of the GNU General Public License v2

EAPI="5"

inherit git-r3 qmake-utils

DESCRIPTION="High level qt bindings to the zeromq networking library"
HOMEPAGE="https://github.com/jonesmz/nzmqt/"
#SRC_URI="https://github.com/jonesmz/nzmqt/archive/version-${PV}.tar.gz"
EGIT_REPO_URI="https://github.com/jonesmz/nzmqt.git"

LICENSE="BSD"
SLOT="0"
KEYWORDS="x86 amd64"
IUSE=""

REQUIRED_USE=""

COMMON_DEPEND="
	dev-qt/qtcore:5
	net-libs/cppzmq
"

DEPEND="
	${COMMON_DEPEND}
"

RDEPEND="
	${COMMON_DEPEND}
"
	
src_configure() {
	eqmake5 src/nzmqt_sharedlib.pro
}

src_install() {
	dolib bin/libnzmqt.so*
	doheader -r include/nzmqt/
        insinto "/usr/$(get_libdir)/pkgconfig"
        newins "${PN}.pc" "${PN}.pc"
}


